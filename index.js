/* RESIZE OPTIMIZE IMAGES */
const Jimp = require('jimp');
const imagesFolder = '/home/ubuntu/metrend-backend/public/posters';
const fs = require('fs');

let images = [];
let width = 297;
let height = 446;
let quality = 100;

let resizeImage = async (images, width, height, quality) => {
    await Promise.all(
        images.map(async imgPath => {
            const image = await Jimp.read(imgPath);
            await image.resize(width, height);
            await image.quality(quality);
            await image.writeAsync(imgPath);
        })
    );
};


fs.readdir(imagesFolder, (err, files) => {
    files.forEach(file => {
        console.log(file);
        images.push("/home/ubuntu/metrend-backend/public/posters/"+file);
    });
    resizeImage(images, width, height, quality).then(r => {
        console.log("All Images Resized");
    });
});

